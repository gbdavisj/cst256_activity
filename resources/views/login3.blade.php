
@extends('layouts.appmaster')
@section('title', 'Login Page')
@section('content')

<h3> Login</h3>

<form method = "post" action = "dologin3">
<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
		<table>
			<tr>
				<td>Username: </td>
				<td><input type = "text" name = "username" /><?php echo $errors->first('username')?></td>
			</tr>

			<tr>
				<td>Password:</td>
				<td><input type = "password" name = "password" /><?php echo $errors->first('password')?></td>
			</tr>
			<tr>
				<td colspan = "2" align = "center">
					<input type = "submit" value = "Login" />
				</td>
		</table>
	</form>
	<!-- Display all the Data Validation Rule Errors -->
<?php
    if($errors->count() != 0)
    {
        echo "<h5>List of Errors</h5>";
        foreach($errors->all() as $message)
        {
            echo $message . "<br/>";
        }
    }
?>
	
	
@endsection