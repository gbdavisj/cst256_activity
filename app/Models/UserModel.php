<?php
namespace App\Models;

class UserModel implements \JsonSerializable
{
    private $id;
    private $username;
    private $password;
    
    //Constructor
    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }
    
    //Setters
    public function setId($id){
        $this->id = $id;
    }
    
    public function setUsername($username){
        $this->username = $username;
    }
    
    public function setPassword($password){
        $this->password = $password;
    }
    
    //Getters
    public function getId(){
        return $this->id;
    }
    
    public function getUsername(){
        return $this->username;
    }
    
    public function getPassword(){
        return $this->password;
    }
    public function jsonSerialize(){
        return get_object_vars($this);
    }

    
}

