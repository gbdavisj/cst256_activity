<?php

namespace App\Http\Middleware;
use App\Services\Utility\MyLogger2;
use Illuminate\Support\Facades\Cache;



use Closure;

class MyTestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $MyLogger = MyLogger2::getLogger();
        $MyLogger->info("Middleware Called");
        
        $user = $request->get('username');
        if ($user != null){
            Cache::put('mydata', 'username', 60);
            $MyLogger->info("Username was put in cache" . $user);
        }
        
        elseif ($user == null){
            if (Cache::get('mydata') != null){
                $MyLogger->info("Username data was obtained by cache");
            }
            else{
                $MyLogger->info("Username was not in cache since field was null");
            }
            
        }
        else{
            $MyLogger->info("Username was not put in cache since field was null");
        }
        
        return $next($request);
    }
}
