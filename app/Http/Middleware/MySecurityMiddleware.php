<?php

namespace App\Http\Middleware;

use App\Services\Utility\MyLogger2;
use Closure;

class MySecurityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $MyLogger = MyLogger2::getLogger();
        $path = $request->path();
        $MyLogger->info("Entering My Security Middleware in handle() at path: " . $path);
        $secureCheck = true;
        if ($request->is('/') || $request->is('login3') || $request->is('dologin3') ||
            $request->is('usersrest') || $request->is('usersrest/*') ||
            $request->is('loggingservice'))
        { $secureCheck = false;}
        $MyLogger->info($secureCheck ? "Security Middleware in handle().....Needs Security" : "Security Middleware in handle().....No Security Required");
        
        if($secureCheck)
        {
            $MyLogger->info("Leaving My Security Middleware in handle() doing a redirect back to login");
            return redirect('/login3');
        }
        
        return $next($request);
    }
}
