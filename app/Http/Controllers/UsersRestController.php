<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DTO;
use App\Services\Business\SecurityService;

class UsersRestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $secServ = new SecurityService();
        $userData = $secServ->getAllUsers();
        $errorCode = "A1";
        $errorMessage = "This is the error message.";
        $dtoData = new DTO($errorCode, $errorMessage, $userData);
        //echo $userData;
        
        return json_encode($dtoData); 
    }

 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $secServ = new SecurityService();
        $userData = $secServ->getUser($id);
        $errorCode = "A1";
        $errorMessage = "This is the error message.";
        $dtoData = new DTO($errorCode, $errorMessage, $userData);
        //echo $userData;
        
        return json_encode($dtoData); 
    }

}
