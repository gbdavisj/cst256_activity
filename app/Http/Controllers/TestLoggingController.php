<?php

namespace App\Http\Controllers;

use App\Services\Utility\ILoggerService;
use Illuminate\Http\Request;

class TestLoggingController extends Controller
{
    protected $logger;
    public function __construct(ILoggerService $logger){
        $this->logger = $logger;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->logger->info("Message from TestLoggingController.index");
    }

}
