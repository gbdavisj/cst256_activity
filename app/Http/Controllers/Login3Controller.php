<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Services\Business\SecurityService;
use App\Services\Business\OrderService;
//use App\Services\Data\CustomerDAO;
//use App\Services\Data\OrderDAO;

class Login3Controller extends Controller
{
    
    public function index(Request $request)
    {
        //used for testing the CustomerDAO before OrderService was created
        //$customerdata = new CustomerDAO(); //data object created
        //$customerdata->addCustomer('Gary','Davis'); //return bool found by the data service. 
        
        //used for testing the OrderDAO before OrderService was created
        //$orderdata = new OrderDAO(); //data object created
        //$orderdata->addOrder('ABC123'); //return bool found by the data service. 
        
        //testing the implementation of the OrderService.
        $orderVer = new OrderService();
        $orderVer->CreateOrder('Tanner', 'Davis', 'Order2');
        
        
        // Validate the Form Data (note will automatically redirect back to Login View if errors)
        //$this->validateForm($request); //had to turn off to test cache
        
        $username = $request->input('username');
        $password = $request->input('password');
        $user = new UserModel($username, $password);
        $userVer = new SecurityService();
        $userVer = $userVer->login($user);
        if ($userVer == true){
            echo "Hello " . $username . "!<br><br>";
            return view('/loginPassed2', compact('user'));
        }
        else{
            return view('/loginFailed');
        }
        
        //used for testing the form post method (uncomment below code for testing)
        //echo "Post Action Output: <br>";
        //echo "Username: " . $username ."<br>";
        //echo "Password: " . $password;
        //echo '<br>';
        //return view('login');
    }
    private function validateForm(Request $request)
    {
        // Setup Data Validation Rules for Login Form
        $rules = ['username' => 'Required | Between:4,10 | Alpha','password' => 'Required | Between:4,10'];
        
        // Run Data Validation Rules
        $this->validate($request, $rules);
    }
    
}
