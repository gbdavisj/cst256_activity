<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;
use Exception;
use App\Models\UserModel;
use App\Services\Business\SecurityService;
//use App\Services\Utility\MyLogger1;
use App\Services\Utility\MyLogger2;

class LoginController extends Controller
{
    
    public function index(Request $request)
    {
        //$MyLogger = new MyLogger1();//Old logger
        $MyLogger = MyLogger2::getLogger();
        try {
            $MyLogger->info("Entering LoginController::index()");
            //$MyLogger->info("Entering LoginController::index(), using MyLogger1");
            //Log::info("Entering LoginController::index()");
            $username = $request->input('username');
            $password = $request->input('password');
            $user = new UserModel($username, $password);
            $userVer = new SecurityService();
            $userVer = $userVer->login($user);
            
            $MyLogger->info("Parameters are: ". implode(",", array("username" => $username, "password" => $password)));
            //$MyLogger->info("Parameters are: ". implode(",", array("username" => $username, "password" => $password)));
            //Log::info("Parameters are: ",array("username" => $username, "password" => $password));
        
            if ($userVer == true){
                $MyLogger->info("Exit LoginController::index() with login passing");
                //$MyLogger->info("Exit LoginController::index() with login passing, using MyLogger1");
                //Log::info("Exit LoginController::index() with login passing");
                echo "Hello " . $username . "!<br><br>";
                return view('/loginPassed2', compact('user'));
            }
            else{
                $MyLogger->info("Exit LoginController::index() with login failing");
                //$MyLogger->info("Exit LoginController::index() with login failing, using MyLogger1");
                //Log::info("Exit LoginController::index() with login failing");
                return view('/loginFailed');
            }
        
        
        //used for testing the form post method (uncomment below code for testing)
        //echo "Post Action Output: <br>";
        //echo "Username: " . $username ."<br>";
        //echo "Password: " . $password;
        //echo '<br>';
        //return view('login');
        
        //trigger exception in a "try" block
        
            
        }
        
        //catch exception
        catch(Exception $e) {
            $MyLogger->error("Exception LoginController::index()" . $e->getMessage());
            //$MyLogger->error("Exception LoginController::index()" . $e->getMessage().",using MyLogger1");
            //Log::error("Exception LoginController::index()" . $e->getMessage());
        }
    }
}
