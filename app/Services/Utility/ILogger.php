<?php
namespace App\Services\Utility;

interface ILogger
{
    public static function getLogger();
    public function debug($Message);
    public function info($Message);
    public function warning($Message);
    public function error($Message);
    
}

