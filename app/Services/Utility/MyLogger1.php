<?php
namespace App\Services\Utility;

//use App\Services\Utility\ILogger;
use Illuminate\Support\Facades\Log;

class MyLogger1 implements ILogger
{

    public static function getLogger(){
        return;
    }
    
    public function debug($classMessage){
        Log::debug($classMessage);
    }   

    public function warning($classMessage){
        Log::warning($classMessage);
    }

    public function error($classMessage){
        Log::error($classMessage);
    }

    public function info($classMessage){
        Log::info($classMessage);
    } 
    
}

