<?php
namespace App\Services\Utility;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class MyLogger2 implements ILogger
{
    private static $instance = null;
    
    public static function getLogger(){
        if (!isset(self::$instance)) {
            self::$instance = new Logger('MyLogger2 Class');
            self::$instance->pushHandler(new StreamHandler('storage/logs/MyLogger2.log', Logger::DEBUG));
        }
        return self::$instance;
    }
    
    public function debug($classMessage){
        $this->getLogger()::debug($classMessage);
    }   

    public function warning($classMessage){
        $this->getLogger()::warning($classMessage);
    }

    public function error($classMessage){
        $this->getLogger()::error($classMessage);
    }

    public function info($classMessage){
        $this->getLogger()::info($classMessage);
    } 
    
}

