<?php
namespace App\Services\Utility;

interface ILoggerService
{
    public function debug($Message);
    public function info($Message);
    public function warning($Message);
    public function error($Message);
    
}

