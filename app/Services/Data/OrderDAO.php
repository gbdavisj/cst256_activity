<?php
namespace App\Services\Data;

class OrderDAO
{
    private $conn;
    public function __construct($conn){
        $this->conn = $conn;
    }
    
    public function addOrder($Product){
        $cust_Id = 17; //hard coded for testing, set to customer ID that isn't in the db to verify rollback works
        return $this->conn->insert('insert into orders (Product, Customer_Id) values (?, ?)', [$Product, $cust_Id]);
    }
}

