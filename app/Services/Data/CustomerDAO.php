<?php
namespace App\Services\Data;

class CustomerDAO
{    
    private $conn;
    public function __construct($conn){
        $this->conn = $conn;
    }
    
    public function addCustomer($firstName, $lastName){
        return $this->conn->insert('insert into customer (First_Name, Last_Name) values (?, ?)', [$firstName, $lastName]);
    }
}

