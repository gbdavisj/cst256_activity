<?php
namespace App\Services\Data;

use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Log;
use PDOException;
use App\Models\UserModel;
//use App\Services\Utility\MyLogger1;
use App\Services\Utility\MyLogger2;

class SecurityDAO
{
    function findByUser(UserModel $user){
        $MyLogger = MyLogger2::getLogger();
        //$MyLogger = new MyLogger1();
        
        $MyLogger->info("Entering SecurityDAO::findByUser(User)");
        //$MyLogger->info("Entering SecurityDAO::findByUser(User), using MyLogger1");
        //Log::info("Entering SecurityDAO::findByUser(User)");
        try {
            $user = DB::table('users')->where('USERNAME', $user->getUsername())->where('PASSWORD',$user->getPassword());
            if ($user->count() > 0){
                
                $MyLogger->info("Exit SecurityDAO::findByUser(User) with return true");
                //$MyLogger->info("Exit SecurityDAO::findByUser(User) with return true, using MyLogger1");
                //Log::info("Exit SecurityDAO::findByUser(User) with return true");
                return true;
            }
            else{
                $MyLogger->info("Exit SecurityDAO::findByUser(User) with return true");
                //$MyLogger->info("Exit SecurityDAO::findByUser(User) with return true, using MyLogger1");
                //Log::info("Exit SecurityDAO::findByUser(User) with return false");
                return false;
            }
        } 
        catch (PDOException $e) {
            $MyLogger->error("Exception SecurityDAO::findByUser(User)" . $e->getMessage());
            $MyLogger->info("Exit SecurityDAO::findByUser(User) with PDOException and return false");
            
            //$MyLogger->error("Exception SecurityDAO::findByUser(User)" . $e->getMessage().", using MyLogger1");
            //$MyLogger->info("Exit SecurityDAO::findByUser(User) with PDOException and return false, using MyLogger1");
            //Log::error("Exception SecurityDAO::findByUser(User)" . $e->getMessage());
            //Log::info("Exit SecurityDAO::findByUser(User) with PDOException and return false");
            return false;
        } 
    }
    
    function findAllUsers(){
        $users = DB::table('users')->select('users.*')->get();
        $userObj = array();
        foreach ($users as $user){
            $tUser = new UserModel($user->USERNAME, $user->PASSWORD);
            $tUser->setId($user->ID);
            array_push($userObj, $tUser); 
        }
        return json_encode($userObj);
    }
    
    
    function findUserByID($id){
        $users = DB::table('users')->select('users.*')
        ->where('users.id','=', $id)
        ->get();
        $userObj = array();
        array_push($userObj, $users); 
        
        return json_encode($userObj);
    }
    

    
}

