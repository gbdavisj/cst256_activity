<?php
namespace App\Services\Business;

//use Illuminate\Support\Facades\Log;
use App\Models\UserModel;
use App\Services\Data\SecurityDAO;
//use App\Services\Utility\MyLogger1;
use App\Services\Utility\MyLogger2;

class SecurityService
{

    public function login(UserModel $user){
        $MyLogger = MyLogger2::getLogger();
        //$MyLogger = new MyLogger1();
        
        $MyLogger->info("Entering SecurityService::login(User)");
        //$MyLogger->info("Entering SecurityService::login(User), using MyLogger1");
        //Log::info("Entering SecurityService::login(User)");
        $userdata = new SecurityDAO(); //data object created
        $userReturn = $userdata->findByUser($user); //return bool found by the data service.
        
        $MyLogger->info("Exit SecurityService::login(User)");
        //$MyLogger->info("Exit SecurityService::login(User), using MyLogger1");
        //Log::info("Exit SecurityService::login(User)");
        return $userReturn;
    }
    
    
    function getAllUsers(){
        $userdata = new SecurityDAO();
        return $userdata->findAllUsers();
    }
    
    function getUser($id){
        $userdata = new SecurityDAO();
        return $userdata->findUserByID($id);
    }
    
    
}

