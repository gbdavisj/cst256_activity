<?php
namespace App\Services\Business;
use Illuminate\Support\Facades\DB;
use App\Services\Data\CustomerDAO;
use App\Services\Data\OrderDAO;

class OrderService
{
    
    public function CreateOrder($firstName, $lastName, $Product){
        $conn = DB::connection('mysql');
        $customerdata = new CustomerDAO($conn);
        $orderdata = new OrderDAO($conn);
        
        //
        //Instructions specify mysqli::autocommit(FALSE) but this method could not be found within the connection, only commit.
        //
        
        //begin the transaction so we know what to commit if no errors or rollback if there are errors
        $conn->beginTransaction();
        
        //these insert methods will return true or false depending if the transaction was successful or not.
        $cust = $customerdata->addCustomer($firstName,$lastName);
        $ord = $orderdata->addOrder($Product);
        
        //commit the changes since the transaction for both were successful
        if ($cust == true && $ord == true){
            $conn->commit();
        }
        //Transaction error occured in either one or both databases.
        else{
            $conn->rollBack();
        }
        
    }
}

